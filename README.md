# CSS Transparent Backgrounds #

Styles to add transparent backgrounds using repeating 1x1 pixel PNG images, all contained within one file.

The images are Base64 encoded within the style sheet, so no image files are needed.

White and black with transparency from 5% to 95% is available.

## Usage ##
```
<!-- A div with a 15% white transparent background -->
<div class="trans-white-15"></div>

<!-- A div with a 45% black transparent background -->
<div class="trans-black-45"></div>
```